# SAE PHP

**SAE du 4ème semestre concernant PHP**

## Membres du groupe
**BOISSAY Gatien (développeur)**
**BROYARD Ethan (développeur)**

-------------
## Lancement et Fonctionnement de l'application 

#### Lancement de l'application web :arrow_forward:

- Récupérer le lien https sur GitLab et faites **git clone <lien>** sur votre machine.
- Placez vous à la racine du projet et faite **php -S localhost:8000** puis suivez le lien.
- Faites attention a bien modifier le fichier connectBD.php en mettant vos identifiants à votre base de données en suivant les différents exemples.
- Ensuite modifié l'url de la page Web pour obtenir: **http://localhost:8000/templates/Connexion.html**


#### Insertion des données de tests :arrow_down:
    
- Entrez dans votre base de donnée est insérez les fichiers suivants:
  - **source quiz.sql**
  - **source insert.sql**


#### Les identifiants de connexion :bust_in_silhouette:


| **Identifiant** | **Mdp** |
|-----------------|---------|
|        4        |   123   |


#### Navigation :earth_africa:
La navigation se fait avec les différents boutons présents sur les pages une fois la connection effectuée.
Néanmoins il n'y a pas de bouton revenir en arrière, cela se gère avec le retour du navigateur.

-------

## Outils utilisés :hammer:
* Gitlab (Stockage des documents)
* Discord (Communication)
* VSCode (Programmation)

## Langages utilisés :snake:
* HTML - CSS - JavaScript
* PHP
* SQL
