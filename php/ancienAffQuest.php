<!DOCTYPE html>
<html>
<head>
  <title>Liste des questionnaires</title>
  <link rel="stylesheet" href="../css/bootstrap.min.css">
</head>
<body>
  <div class="container mt-5">
    <h1>Liste des questionnaires</h1>

    <?php
    // Connexion à la base de données

    require("connexionBD.php");
    $connexion=connect_bd();
    
    //generer_bd_to_PHP();
    //$afficheQuestionnaire="";
    //$afficheQuestionnaire.=get_questionnaire();
    

    // Récupération de la liste des questionnaires
    $sql = "SELECT * FROM QUESTIONNAIRE";
    if(!$connexion->query($sql)) echo "Pb d'accès au CARNET";
    //Affichage des questionnaires dans un tableau HTML
   
    // echo "<table>";
    // echo "<tr><th>ID</th><th>Nom</th><th>Thème</th><th>Nombre de questions</th></tr>";
    // foreach ($connexion->query($sql) as $row)
    //     $id = $row["IDQUESTIONNAIRE"];
    //     $nom = $row["NOMQUESTIONNAIRE"];
    //     $theme = $row["THEMEQUESTIONNAIRE"];
    //     $nb_questions = $row["NOMBREQUESTION"];
    //     echo "<tr><td>$id</td><td>$nom</td><td>$theme</td><td>$nb_questions</td></tr>";
    
    // echo "</table>";
   
    else{
      ?>
         <form action="rechercheQuestionnaire.php" method="GET">
          <select name="IDQUESTIONNAIRE">
          <?php
          foreach ($connexion->query($sql) as $row)
          if(!empty($row['NOMQUESTIONNAIRE']))
          echo "<option value='".$row['IDQUESTIONNAIRE']."'>"
          .$row['THEMEQUESTIONNAIRE']." ".$row['NOMQUESTIONNAIRE']." ".$row['NOMBREQUESTION']."</option>\n";
          echo $afficheQuestionnaire;
          ?>
         </select>
         <input type="submit" value="Lancer le Quiz">
         </form>
         <?php 
          } ?>
        <a href="/templates/addQuestionnaire.html/" value="Ajouter un Questionnaire">Ajouter un Questionnaire</a>
        <a href="/php/exportJSON.php/" value="Export JSON">Exporter en JSON</a>
        <form method="post" enctype="multipart/form-data" action="importJSON.php">
        <label for="json_file">Sélectionner un fichier JSON à importer:</label>
        <input type="file" id="json_file" name="json_file" accept=".json">
        <input type="submit" value="Télécharger">
        </form>


   
  </div>
</body>
</html>