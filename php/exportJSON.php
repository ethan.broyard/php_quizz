<?php
require_once('connexionBD.php');
$connexion=connect_bd();
$sql="SELECT * from QUESTIONNAIRE ";
$data=$connexion->query($sql);
$questionnaires=array();

//while($questionnaire=$data->fetch(PDO::FETCH_OBJ)){
  while($questionnaire=$data->fetch(PDO::FETCH_ASSOC)){
    $questionnaires[]=$questionnaire;
  }

$sql2="SELECT * from QUESTION ";
$data2=$connexion->query($sql2);
$questions=array();
  
//while($question=$data->fetch(PDO::FETCH_OBJ)){
    while($question=$data2->fetch(PDO::FETCH_ASSOC)){
      $questions[]=$question;
    }

$sql3="SELECT * from REPONSE ";
$data3=$connexion->query($sql3);
$reponses=array();
      
//while($reponse=$data->fetch(PDO::FETCH_OBJ)){
    while($reponse=$data3->fetch(PDO::FETCH_ASSOC)){
        $reponses[]=$reponse;
    }

$sql4="SELECT * from USER ";
$data4=$connexion->query($sql4);
$users=array();
          
//while($users=$data->fetch(PDO::FETCH_OBJ)){
    while($user=$data4->fetch(PDO::FETCH_ASSOC)){
        $users[]=$user;
    }


$res=array();
$res[]=$questionnaires;
$res[]=$questions;
$res[]=$reponses;
$res[]=$users;
//print_r($questionnaires);
$json = json_encode($res, JSON_PRETTY_PRINT);


// echo 'json :<br>'.PHP_EOL;
// print($json);
#echo '<br>'.PHP_EOL;
#echo 'décodage du json : <br>'.PHP_EOL;
#print_r(json_decode($json, true));

// Enregistrer la chaîne de caractères JSON dans un fichier
$file_path = '../data/donnee.json';
file_put_contents($file_path, $json);

header("Location: ../../php/affichageQuestionnaire.php?export=True");