<?php

    class Reponse{
            protected int $idreponse;
            protected String $nomreponse;
            protected String $intitulereponse;
            protected boolean $bonne;
            protected int $idquestion;

            public function __construct($idreponse,$nomreponse,$intitulereponse,$bonne,$idquestion){
                $this->idreponse=(int)$idreponse;
                $this->nomReponse=(String)$nomReponse;
                $this->intitulereponse=(String)$intitulereponse;
                $this->bonne=(boolean)$bonne;
                $this->idquestion=(int)$idquestion;
                
            }

            public function equals(Reponse $ques){
                return ($this->getIdReponse() == $ques->getIdReponse());
            }
            
            public function getIdReponse(){
                return $this->idreponse;
            }
            
            public function getNomReponse(){
                return $this->nomreponse;
              }
            
            public function getintitule(){
                return $this->intitulereponse;
              }
            public function getBonne(){
                return $this->bonne;
            }
            public function getIdQuestion(){
                return $this->idquestion;
            }

            
            public function __toString(){
                  
                return 'Reponse: idReponse=' . $this->getIdReponse().', nomReponse='.$this->getNomReponse().', intitule='.$this->getintitule().', Les Reponses: '.$this->getIdQuestion()." ";
            }
        }
            
            
    /* Test : */
$Reponsenaire=new Questionnaire(234,"Talon","Qui faire peur",["question1","question2"]);
var_dump($questionnaire);
echo "<br/>";
echo $questionnaire;

?> 