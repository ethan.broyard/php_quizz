<?php

    class Question{
            protected int $idquestion;
            protected String $nomquestion;
            protected String $intitule;
            protected int $typequestion;
            protected array $listereponses;
            protected int $idquestionnaire;
            

            public function __construct($idquestion,$nomquestion,$intitule,$typequestion,$listereponses,$idquestionnaire){
                $this->idquestion=(int)$idquestion;
                $this->nomquestion=(String)$nomquestion;
                $this->intitule=(String)$intitule;
                $this->typequestion=(int)$typequestion;
                $this->listereponses=(array)$listereponses;
                $this->idquestionnaire=(int)$idquestionnaire;
                
            }

            public function equals(Question $ques){
                return ($this->getIdquestion() == $ques->getIdquestion());
            }
            
            public function getIdquestion(){
                return $this->idquestion;
            }
            
            public function getNomquestion(){
                return $this->nomquestionn;
              }
            
            public function getintitule(){
                return $this->intitule;
              }
            public function getTypequestion(){
                return $this->typequestion;
            }
            public function getListeReponses(){
                return $this->listereponses;
            }
            public function getIdquestionnaire(){
                return $this->idquestionnaire;
            }

            public function setIdQuestion($idquestion) {
                $this->idquestion = (int)$idquestion;
            }
            
            public function setNomQuestion($nomquestion) {
                $this->nomquestion = (string)$nomquestion;
            }
            
            public function setIntitule($intitule) {
                $this->intitule = (string)$intitule;
            }
            
            public function setTypeQuestion($typequestion) {
                $this->typequestion = (int)$typequestion;
            }
            
            public function setListeReponses($listereponses) {
                $this->listereponses = (array)$listereponses;
            }
            
            public function setIdQuestionnaire($idquestionnaire) {
                $this->idquestionnaire = (int)$idquestionnaire;
            }
            
            public function __toString(){
                  
                return 'Question: idquestion=' . $this->getIdquestionnaire().', nomquestion='.$this->getNomquestionnaire().', intitule='.$this->getintitule().', listereponses='.$this->getListeReponses().', Les Questions: '.$this->getListquestion()." ";
            }
        }
            
            
    /* Test : */
$question=new Question(24,"Talon","je fais peur",1,2);
var_dump($question);
echo "<br/>";
echo $question;

?> 