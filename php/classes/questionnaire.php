<?php

    class Questionnaire{
            protected int $idquestionnaire;
            protected String $nomquestionnaire;
            protected String $themequestionnaire;
            protected int $nombrequestion;
            protected array $listequestion;

            public function __construct($idquestionnaire,$nomquestionnaire,$themequestionnaire,$nombrequestion,$listequestion){
                $this->idquestionnaire=(int)$idquestionnaire;
                $this->nomquestionnaire=(String)$nomquestionnaire;
                $this->themequestionnaire=(String)$themequestionnaire;
                $this->nombrequestion=(int)$nombrequestion;
                $this->listequestion=(array)$listequestion;
            }

            public function equals(Questionnaire $quest){
                return ($this->getIdquestionnaire() == $quest->getIdquestionnaire());
            }
            
            public function getIdquestionnaire(){
                return $this->idquestionnaire;
            }
            
            public function getNomquestionnaire(){
                return $this->nomquestionnaire;
              }
            
            public function getThemequestionnaire(){
                return $this->themequestionnaire;
              }
            public function getNombreQuestion(){
                return $this->nombrequestion;
            }
            public function getListquestion(){
                return $this->listequestion;
            }
            public function setIdQuestionnaire($idquestionnaire) {
                $this->idquestionnaire = (int)$idquestionnaire;
            }
            
            public function setNomQuestionnaire($nomquestionnaire) {
                $this->nomquestionnaire = (string)$nomquestionnaire;
            }
            
            public function setThemeQuestionnaire($themequestionnaire) {
                $this->themequestionnaire = (string)$themequestionnaire;
            }
            
            public function setNombreQuestions($nombrequestion) {
                $this->nombrequestion = (int)$nombrequestion;
            }
            
            public function setListeQuestions($listequestion) {
                $this->listequestion = (array)$listequestion;
            }
            
            public function __toString(){
                  
                return 'Questionnaire: idquestionnaire=' . $this->getIdquestionnaire().', nomquestionnaire='.$this->getNomquestionnaire().', themequestionnaire='.$this->getThemequestionnaire().', nombrequestion='.$this->getNombreQuestion().', Les Questions: '.$this->getListquestion()." ";
            }
        }
            
            
    /* Test : */
$questionnaire=new Questionnaire(234,"Talon","Qui faire peur","film",["question1","question2"]);
var_dump($questionnaire);
echo "<br/>";
echo $questionnaire;

?> 