<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title>Formulaire Identity</title>
        <link rel="stylesheet" href="style.css">
    </head>
    
    <body>
        <section id="Formulaire">
            <h1>Formulaire Identity</h1>
            <form name ="f1" action="harry.html" method="GET">
            <fieldset>
                <legend>Informations Personnelles</legend>
                <p>Nom:<input type="text" id="Nom" name="Nom" pattern="[a-z A-Z]{1,20}([-][a-z A-Z]{1,20})?" placeholder="Ex:Monsieur HERALD DE LA FAILLE"></input></p>
                <p>Prenom:<input type="text" id="Prenom"name="Prenom" pattern="[a-z A-Z]{1,20}([-][a-z A-Z]{1,20})?" placeholder="Ex:Thierry"></input></p>
                <p>Email:<input type="email" id="email" name="email" pattern="[a-z A-Z -._%]+@[a-z A-Z]+\.[a-z A-Z]{1,50}" placeholder="Ex:@gmail,@laposte"></input></p>
                <p>Telephone<input type="tel" id="tele" name="tele" pattern="[\d]{10,12}" placeholder="Ex: 0 1 2 3 4 5" title=«Entrez un telephone entre 10 et 12 chiffres »/></p>
                <p>Date de Naissance<input type="date"  id="date" name="date" placeholder="Ex: Jour/Mois/Annee"/></p>
        
            </fieldset>
        
            <br/>
            <fieldset>
                <legend>Langues que vous maitrisé</legend>
                <p>Anglais<input type="checkbox" name="Langue" value="anglais" ></input></p>
                <p>Allemand<input type="checkbox" name="Langue" ></input></p>
                <p>Français<input type="checkbox" name="Langue" ></input></p>
                <p>Italien<input type="checkbox" name="Langue">  </input></p>
            </fieldset>
            <br/>
            <fieldset>
                <legend>Genre</legend>
                <p>Homme</p><input type="radio" id="Genre" name="Genre">  </input>
                <p>Femme</p><input type="radio" id="Genre" name="Genre">  </input>
            </fieldset>
            <br/>
            <input id= valider type="submit" name="Changer" value="Confirmez les Informations "  onclick =verif()></input>
        </section>
        <br/>
        

        </form>
    </body>
</html>